package assignments.assignment2;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        this.nama = nama;
        this.bobot = bobot;
        butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        butirPenilaian[idx] = butir;
    }

    public String getNama() {
        return nama;
    }

    public double getRerata() {
        int length = 0;
        double total = 0;
        for (ButirPenilaian butir: butirPenilaian) {
            if (butir != null) {
                total += butir.getNilai();
                length++;
            }
        }
        if (length == 0) {
            return 0.0;
        }
        return total / length;
    }

    public double getNilai() {
        return this.getRerata() * bobot / 100;
    }

    public String getDetail() {
        String hasil = "";
        hasil += "--- " + nama + " (" + bobot + "%) ---\n";
        for (int i = 0; i < butirPenilaian.length; i++) {
            if (butirPenilaian[i] == null) {
                continue;
            }
            hasil += nama + " " + (i + 1) + ": " + butirPenilaian[i].toString();
        }
        hasil += "Rerata: " + String.format("%.2f", this.getRerata()) + "\n";
        hasil += "Kontribusi nilai akhir: " + String.format("%.2f", this.getNilai()) + "\n";
        return hasil;
    }

    @Override
    public String toString() {
        return "Rerata " + nama + ": " + String.format("%.2f",this.getRerata());
    }

}
