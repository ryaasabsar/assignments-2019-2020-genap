package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        for (KomponenPenilaian komponen : komponenPenilaian) {
            if (komponen == null) {
                continue;
            }
            if (komponen.getNama().equals(namaKomponen)) {
                return komponen;
            }
        }
        return null;
    }

    public String getNpm() {
        return npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        String hasil = "";
        for (KomponenPenilaian komponen: komponenPenilaian) {
            hasil += komponen.toString() + "\n";
        }
        hasil += this.getNilaiAkhirPrompt();
        return hasil;
    }

    public double getNilaiAkhir() {
        double nilai = 0;
        for (KomponenPenilaian komponen: komponenPenilaian) {
            nilai += komponen.getNilai();
        }
        return nilai;
    }

    public String getNilaiAkhirPrompt() {
        String hasil = "";
        double nilai = this.getNilaiAkhir();
        hasil += "Nilai akhir: " + String.format("%.2f", nilai) + "\n";
        hasil += "Huruf: " + Mahasiswa.getHuruf(nilai) + "\n";
        hasil += Mahasiswa.getKelulusan(nilai) + "\n";
        return hasil;
    }

    public String toString() {
        return npm + " - " + nama;
    }

    public String getDetail() {
        String hasil = "";
        for (KomponenPenilaian komponen: komponenPenilaian) {
            hasil += komponen.getDetail() + "\n";
        }
        hasil += getNilaiAkhirPrompt();
        return hasil;
    }

    @Override
    public int compareTo(Mahasiswa other) {
        return this.getNpm().compareTo(other.getNpm());
    }
}
