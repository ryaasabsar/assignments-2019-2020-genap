package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        return kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
    }

    public Mahasiswa getMahasiswa(String npm) {
        for (Mahasiswa mhsw : mahasiswa) {
            if (mhsw == null) {
                continue;
            }
            if (mhsw.getNpm().equals(npm)) {
                return mhsw;
            }
        }
        return null;
    }

    public String rekap() {
        String hasil = "";
        for (Mahasiswa mhsw : mahasiswa) {
            hasil += mhsw.toString() + "\n";
            hasil += mhsw.rekap();
            hasil += "\n";
        }
        return hasil;
    }

    public String toString() {
        return kode + " - " + nama;
    }
}
