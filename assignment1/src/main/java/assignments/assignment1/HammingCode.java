package assignments.assignment1;

import java.util.ArrayList;
import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /** 
     * Encoding Hamming Code Method.
     * return Hamming Code
     */
    public static String encode(String data) {
        ArrayList<String> codeList = new ArrayList<String>();
        int redundant = searchR(data.length());
        int dataLength = data.length();
        for (int i = 0; i < dataLength; i++) {
            codeList.add(data.substring(i,i + 1));
        }
        for (int i = 0; i < redundant; i++) {
            codeList.add(power(2,i) - 1,"0");
        }
 
        int temp = 0;
        int bit = 0;
        int counter = 0;
        for (int i = 0; i < redundant; i++) {
            bit = power(2,i);
            for (int index = bit - 1; index < codeList.size(); index++) {
                int element = Integer.parseInt(codeList.get(index));
                temp += element;
                counter++;
                if (bit == counter) {
                    index += bit;
                    counter = 0;
                }
            }
            if (temp % 2 == 0) {
                codeList.set(bit - 1, "0");
            } else {
                codeList.set(bit - 1, "1");
            }
            temp = 0;
            counter = 0;
        }

        String result = "";
        for (int i = 0; i < codeList.size(); i++) {
            result += codeList.get(i);
        }

        return result;
    }

    /** 
     * Decoding Hamming Code.
     * return Raw Data
     */
    public static String decode(String code) {
        ArrayList<String> codeList = new ArrayList<String>();
        int codeLength = code.length();

        codeList.add("x");
        for (int i = 0; i < codeLength; i++) {
            codeList.add(code.substring(i,i + 1));
        }

        int parityLoc = 0;
        int temp = 0;
        int bit = 0;
        int counter = 0;
        int powerCounter = 0;
        int falseBit = 0;
        while (parityLoc <= codeLength) {
            bit = power(2,powerCounter);
            parityLoc = bit;
            for (int index = parityLoc; index < codeList.size(); index++) {
                int element = Integer.parseInt(codeList.get(index));
                temp += element;
                counter++;
                if (bit == counter) {
                    index += bit;
                    counter = 0;
                }
            }
            if (temp % 2 != 0) {
                falseBit += parityLoc;
            } 
            temp = 0;
            counter = 0;
            powerCounter++;
        }

        if (codeList.get(falseBit).equals("0")) {
            codeList.set(falseBit, "1");
        } else {
            codeList.set(falseBit, "0");
        }
        
        codeList.set(0, "x");    
        counter = 0;
        parityLoc = 1;
        powerCounter = 0;
        while (power(2,powerCounter) < codeLength) {
            codeList.set(power(2,powerCounter), "x");
            powerCounter++;
        }

        String result = "";
        for (int i = 1; i < codeList.size(); i++) {
            if (!codeList.get(i).equals("x")) {
                result += codeList.get(i);
            }
        }

        return result;
    }

    /** 
     * Find out how much redundant bit needed.
     * return Redundant bit count
     */
    public static int searchR(int length) {
        for (int r = 0; r <= Math.sqrt(length) + 2; r++) {
            if (power(2,r) >= length + r + 1) { 
                return r;
            }
        }
        return 0;
    }

    /** 
     * Math power method.
     * return result of power
     */
    public static int power(int base, int times) {
        if (times == 0) {
            return 1;
        }
        return base * power(base,times - 1);
    }

    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}