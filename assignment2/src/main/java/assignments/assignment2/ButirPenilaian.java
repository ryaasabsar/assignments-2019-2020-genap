package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
        this.nilai = nilai;
        this.terlambat = terlambat;
    }

    public double getNilai() {
        if (!terlambat) {
            return nilai;
        }
        return nilai - nilai * PENALTI_KETERLAMBATAN / 100;
    }

    public boolean getTerlambat() {
        return terlambat;
    }

    @Override
    public String toString() {
        String hasil = "" + String.format("%.2f", this.getNilai());
        if (terlambat) {
            hasil += " (T)";
        }
        return hasil;
    }
}
